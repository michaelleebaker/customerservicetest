define([
    'uiElement',
    'Magento_Customer/js/customer-data',
    'ko',
    'domReady!'
], function(Component, customerData, ko) {
    'use strict';

    return Component.extend({

        initialize: function () {
            this._super();

        },

        quoteId: ko.observable(customerData.get('cart')().quote_id)

    });

});
