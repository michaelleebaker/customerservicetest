<?php
declare(strict_types=1);

namespace Polywood\CustomerService\Plugin;

use Magento\Checkout\CustomerData\Cart;
use Magento\Framework\Exception\LocalizedException;
use Magento\Checkout\Model\Session;

/**
 * Class AddQuoteIdToCart
 * @package Polywood\CustomerService\Plugin
 */
class AddQuoteIdToCart {


    /**
     * @var Session
     */
    protected $checkoutSession;


    /**
     * Initialize Plugin
     *
     * @param Session $checkoutSession
     */
    public function __construct(
        Session $checkoutSession
    ) {
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * Magento_Checkout/js/model/quote only works in checkout pages, so adding quote id
     * to the customer cart data which is accessible everywhere
     *
     * @param Cart $subject
     * @param $result
     * @return array
     */
    public function afterGetSectionData(Cart $subject, $result)
    {
        try{

            $quote = $this->checkoutSession->getQuote();

            if(isset($quote)){
                $result['quote_id'] = $quote->getId();

                return $result;
            }

        }catch (LocalizedException $e) {
            $response = [
                'errors' => true,
                'message' => $e->getMessage(),
            ];

            return $result->setData($response);
        }

        return $result;
    }
}

