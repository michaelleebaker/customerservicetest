This module uses js to display the "customer service id" aka - quote id to the customer on the frontend on all pages.  After adding items to the cart, the user can refresh the page and see the "Customer Service Id: XX" displayed text at the bottom of the screen. It injects the data in the footer container on all pages except checkout, where it is injected at the end of the content block since there is no footer container on the default checkout layout. Since I developed this on the luma theme, I placed it at the bottom of the site. 
However, if I were developing this for the current polywood theme, it looks like a good choice would be to place it inside the top right "Customer Service" menu dropdown. 

In the admin portion, I chose to leverage some existing magento ui components and blocks by extending and customizing as needed. 

The "Cart Totals" section on this page will display discounts and shipping totals as well. I have also added any applied rule ids for display and the cart last updated date.

If the customer is logged in, I have also added some extra helpful information about the customer at the bottom of the view customer cart page in the admin. If time allowed, I would consider adding related product information to this screen so that the customer service rep could possibly upsell while on the phone. 
