<?php
/**
 * Copyright: 2021 (c) Polywood
 *
 * @author Michael Baker
 * @copyright Polywood, 08/21/21
 * @package Polywood
 **/

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Polywood_CustomerService',
    __DIR__
);
