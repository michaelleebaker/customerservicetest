<?php
declare(strict_types=1);

namespace Polywood\CustomerService\Model\Cart;

use Magento\Store\Model\StoreManager;
use Magento\Ui\DataProvider\ModifierPoolDataProvider;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\Modifier\PoolInterface;
use Magento\Checkout\Model\ResourceModel\Cart;


/**
 * Class CartDataProvider
 * @package Polywood\CustomerService\Model\Cart\CustomerService
 */
class CartDataProvider extends ModifierPoolDataProvider
{
    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @var DataPersistorInterface
     */
    private $_dataPersistor;


    /**
     * @var Object
     */
    private $_storeManager;


    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $CustomerServiceCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     * @param StoreManager $storeManager
     * @param PoolInterface|null $pool
     */
    public function __construct(
        String $name,
        String $primaryFieldName,
        String $requestFieldName,
        Cart $cart,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = [],
        StoreManager $storeManager,
        PoolInterface $pool = null

    ) {
        $this->_dataPersistor = $dataPersistor;
        $this->cart = $cart;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data, $pool);
        $this->meta = $this->prepareMeta($this->meta);
        $this->_storeManager = $storeManager;
    }


    /**
     * Prepares Meta
     *
     * @param array $meta
     * @return array
     */
    public function prepareMeta(array $meta)
    {
        return $meta;
    }


    /**
     * Get cart data
     *
     * @return array
     */
    public function getData()
    {
        //return empty array here because ui form component expects collection to filter
        //and load data in ui_component form xml <requestFieldName>entity_id</requestFieldName>
        return [];
    }

    /**
     * This is needed since we are not passing loaded collection data to this form
     * @param \Magento\Framework\Api\Filter $filter
     * @return mixed|void|null
     */
    public function addFilter(\Magento\Framework\Api\Filter $filter)
    {
        return null;
    }


}


