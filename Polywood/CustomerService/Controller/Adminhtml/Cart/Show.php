<?php
declare(strict_types=1);
namespace Polywood\CustomerService\Controller\Adminhtml\Cart;


use Magento\Backend\App\Action;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\QuoteFactory;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\Controller\ResultFactory;


/**
 * Class Show
 * Handles Lookup Customer Cart form submission and adds data to needed blocks
 * @package Polywood\CustomerService\Controller\Adminhtml\Cart
 */
class Show extends Action
{

    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var QuoteFactory
     */
    private $quoteFactory;


    /**
     * Show constructor.
     * @param Action\Context $context
     * @param QuoteFactory $quoteFactory
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Action\Context $context,
        QuoteFactory $quoteFactory,
        PageFactory  $pageFactory
    ) {

        $this->quoteFactory = $quoteFactory;
        $this->pageFactory = $pageFactory;

        parent::__construct($context);

    }


    /**
     * Show action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return ResultInterface
     * @throws LocalizedException
     */
    public function execute()
    {
        $cartId = $this->getRequest()->getParam('cart_id');

        $quote = null;
        if ($cartId) {
            $quote = $this->quoteFactory->create()->loadByIdWithoutStore($cartId);
        }

        if(!$quote->hasData()){
            //redirect back to form if quote not found
            $this->messageManager->addWarningMessage(__("Could not find quote with that id."));
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            $resultRedirect->setUrl($this->_redirect->getRefererUrl());
            return $resultRedirect;
        }

        $resultPage = $this->pageFactory->create();

        $resultPage->setActiveMenu('Polywood_CustomerService::form');
        $resultPage->getConfig()->getTitle()->prepend(__('View Customer Cart'));


        /**
         * Pass the quote to the needed block files
         */
        /** @var Template $cartBlock */
        $cartBlock = $resultPage->getLayout()->getBlock('polywood.customerservice.view.cart');
        $cartBlock->setData('quote', $quote);

        /** @var Template $totalsBlock */
        $totalsBlock = $resultPage->getLayout()->getBlock('polywood.customerservice.view.totals');
        $totalsBlock->setData('quote', $quote);


        return $resultPage;

    }

    /**
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Polywood_CustomerService::cart');
    }


}

