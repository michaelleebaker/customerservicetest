<?php
declare(strict_types=1);

namespace Polywood\CustomerService\Controller\Adminhtml\Cart;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Form
 * @package Polywood\CustomerService\Controller\Adminhtml\Cart
 */
class Form extends Action implements HttpGetActionInterface
{
    /**
     * @var PageFactory
     */
    private $_pageFactory;


    /**
     * Constructor
     *
     * @param Context $context
     * @param PageFactory $rawFactory
     */
    public function __construct(
        Context $context,
        PageFactory $rawFactory
    ) {
        $this->_pageFactory = $rawFactory;

        parent::__construct($context);
    }


    /**
     * Add the main Admin Grid page
     *
     * @return Page
     */
    public function execute()
    {
        $resultPage = $this->_pageFactory->create();
        $resultPage->setActiveMenu('Polywood_CustomerService::form');
        $resultPage->getConfig()->getTitle()->prepend(__('Polywood CustomerService'));

        return $resultPage;
    }

    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Polywood_CustomerService::cart');
    }


}
