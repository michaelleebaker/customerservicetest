<?php
declare(strict_types=1);

namespace Polywood\CustomerService\Block\Adminhtml\Cart\View;

use Magento\Framework\Pricing\PriceCurrencyInterface;

/**
 * Class Totals
 * Customize display of cart totals
 * @package Polywood\CustomerService\Block\Adminhtml\Cart\View
 */
class Totals extends \Magento\Sales\Block\Adminhtml\Order\Create\Totals
{

   /**
     * @var \Magento\Quote\Model\Quote
     */
    protected $quote = null;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Model\Session\Quote $sessionQuote
     * @param \Magento\Sales\Model\AdminOrder\Create $orderCreate
     * @param PriceCurrencyInterface $priceCurrency
     * @param \Magento\Sales\Helper\Data $salesData
     * @param \Magento\Sales\Model\Config $salesConfig
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Model\Session\Quote $sessionQuote,
        \Magento\Sales\Model\AdminOrder\Create $orderCreate,
        PriceCurrencyInterface $priceCurrency,
        \Magento\Sales\Helper\Data $salesData,
        \Magento\Sales\Model\Config $salesConfig,
        array $data = []
    ) {
        $this->_salesData = $salesData;
        $this->_salesConfig = $salesConfig;
        parent::__construct($context, $sessionQuote, $orderCreate, $priceCurrency, $salesData, $salesConfig, $data);
    }


    /**
     * Get totals
     *
     * @return array
     */
    public function getTotals()
    {
        if ($this->getQuote()->isVirtual()) {
            $totals = $this->getQuote()->getBillingAddress()->getTotals();
        } else {
            $totals = $this->getQuote()->getShippingAddress()->getTotals();
        }
        return $totals;
    }


    /**
     * Get header text
     *
     * @return \Magento\Framework\Phrase
     */
    public function getHeaderText()
    {
        return __('Cart Totals');
    }


    /**
     * Get the quote of the cart from quote object passed from controller
     * instead of from session
     *
     * @return \Magento\Quote\Model\Quote
     */
    public function getQuote()
    {
        if (null === $this->quote) {
            $this->quote = $this->getData('quote');
        }

        return $this->quote;
    }


    /**
     * Get the applied rule ids
     *
     * @return string
     */
    public function getAppliedRuleIds()
    {
        return $this->quote->getAppliedRuleIds();
    }


    /**
     * Get the customer id
     *
     * @return string
     */
    public function getCustomerId()
    {
        return $this->quote->getCustomerId();
    }


    /**
     * Get the customer name
     *
     * @return string
     */
    public function getCustomerName()
    {
        return $this->quote->getCustomerFirstname()  . ' ' .
               $this->quote->getCustomerMiddlename() . ' ' .
               $this->quote->getCustomerLastname();
    }


    /**
     * Get the customer email
     *
     * @return string
     */
    public function getCustomerEmail()
    {
        return $this->quote->getCustomerEmail();
    }


    /**
     * Get the customer ip
     *
     * @return string
     */
    public function getCustomerRemoteIp()
    {
        return $this->quote->getRemoteIp();
    }


    /**
     * Get updated at
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->quote->getUpdatedAt();
    }


    /**
     * Get the quote shipping address
     * @return string
     */
    public function getShippingAddress()
    {
        $address =null;

        $address = $this->quote->getShippingAddress();

        //check quote has address object to return
        if(!$address instanceof \Magento\Quote\Model\Quote\Address ){
            return null;
        }

        $addressString = $address->getStreetFull() . '<br /> ' .
                         $address->getCity() . ', ' .
                         $address->getRegion() . ' ' .
                         $address->getPostcode(). ' ' .
                         $address->getCountry();

        return $addressString;

    }



}
