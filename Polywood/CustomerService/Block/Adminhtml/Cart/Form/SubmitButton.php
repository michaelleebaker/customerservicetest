<?php
declare(strict_types=1);

namespace Polywood\CustomerService\Block\Adminhtml\Cart\Form;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class SubmitButton
 * @package Polywood\CustomerService\Block\Adminhtml\Cart\Form
 */
class SubmitButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Lookup Cart'),
            'class' => 'save primary',
            'data_attribute' => [
                'mage-init' => ['button' => ['event' => 'save']],
                'form-role' => 'save',
            ],
            'sort_order' => 90,
        ];
    }


}
